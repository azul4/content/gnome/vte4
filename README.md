# vte4

Required by gnome-console. Virtual Terminal Emulator widget

https://wiki.gnome.org/Apps/Terminal/VTE

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/gnome/vte4.git
```

